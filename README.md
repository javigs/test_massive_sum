Several tests using different approach to sum up efficiently a huge array of floats.

1. NAIVE (t.c)
2. Using SSE SIMD instructions (t_sse.c)
3. Using AVX SIMD (t_avx.c)
4. Using AVX512 instructions (t_avx512.c) *but doesn't work in my processor*

Other test:
* Unrolling array
* Using ligth threads (not forks)


## RESULS in my comp

```shell
$ clang t.c -O3 -fassociative-math -fno-trapping-math -o t
$ ./t
0.685441s, 729M rows/s, 6965.916504

$ clang t_sse.c -O3 -fassociative-math -fno-trapping-math -o t_sse
$ ./t_sse
0.180930s, 2763M rows/s, 6965.492188

$ clang t_avx.c -O3 -mavx -fassociative-math -fno-trapping-math -o t_avx
$ ./t_avx 
0.111720s, 4475M rows/s, 6967.716797

$ #My processor refuse exec this
$ clang t_avx512.c -O3 -mavx512f -fassociative-math -fno-trapping-math -o t_avx512 
$ ./t_avx512 
Illegal instruction (core dumped)

```