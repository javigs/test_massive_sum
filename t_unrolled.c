#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define N 500000000

float data[N];

double sum(float* d, long int n) {
    double f = 0.0;
    for(int i = 0;i < n; i += 8) {
        f += d[i];
        f += d[i+1];
        f += d[i+2];
        f += d[i+3];
        f += d[i+4];
        f += d[i+5];
        f += d[i+6];
        f += d[i+7];
    }

    for (int i = n / 8 * 8; i < n; i++) {
		f += d[i];
	}

    return f;
}

int main() {
    for(int i = 0; i < N; ++i) 
        data[i] = ((float)rand())/(float)RAND_MAX - 0.5;
    clock_t t = clock();
    float total = sum(data, N);
    t = clock() - t;
    double time = ((double)t)/CLOCKS_PER_SEC;
    printf("%fs, %.0fM rows/s, %f\n", time, N/time/1e6, total);
}
