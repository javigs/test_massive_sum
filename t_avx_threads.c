// Compile with -mavx -lpthread
// This is not parallel execution on several cores.
// It is multithreaded execution, probably on a single core.
// Performance is worse due to overloading.
// Maybe with fork works.

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#if defined(__AVX__)
    #include <immintrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
    #warning NO avx
    #include <x86intrin.h>
#endif

#define NUM_THREADS	8

#define N 500000000
#define V_SIZE 8

typedef struct {
    long start;
    long end;
    float* data;
    double result;
} t_chunk;

float data[N];

void* thread_sum(void *args) {
    t_chunk *chunk = args;
    long start = chunk->start;
    long end = chunk->end;
    float *data = chunk->data;

    chunk->result = 0.0;

    __m256 suma = _mm256_setzero_ps();
    __m256 aux;
    float partial[V_SIZE];

    for (long i = start; i < end; i += V_SIZE) {
    	aux = _mm256_loadu_ps((float *) (data+i));
    	suma = _mm256_add_ps(suma, aux);
    }

    _mm256_storeu_ps((float *) partial, suma);

    // Add partial vector elements
    for (int i = 0; i < V_SIZE; i++) {
    	chunk->result += partial[i];
    }

    pthread_exit(NULL);
}

double sum(float* d, long int n) {
    pthread_t threads[NUM_THREADS];
    t_chunk chunk[NUM_THREADS]; 
	int i, error;
    long start = 0;
    long size = N / NUM_THREADS;
    size -= size % V_SIZE;
    int rest = N - (size * NUM_THREADS);
    double result = 0.0;

	for (i = 0; i < NUM_THREADS; i++) {
        chunk[i].start = start;
        chunk[i].end = start + size;
        chunk[i].data = d;
		error = pthread_create(&threads[i], NULL, thread_sum, (void *)&chunk[i]);
		if (error){
			printf("ERROR code: %d\n", error);
			exit(-1);
		}
        start = (i + 1) * size;
	}

	for (i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}

	for (i = 0; i < NUM_THREADS; i++) {
		result += chunk[i].result;
	}

    for (long j = N - rest; j < N; j++) {
        result += d[j];
    }

    return result;
}

int main() {
    for(int i = 0; i < N; i++)
        data[i] = ((float)rand())/(float)RAND_MAX - 0.5;

    clock_t t = clock();
    float total = sum(data, N);
    t = clock() - t;
    double time = ((double)t)/CLOCKS_PER_SEC;
    printf("%fs, %.0fM rows/s, %f\n\n", time, N/time/1e6, total);

    return 0;
}
