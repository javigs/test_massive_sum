#include <time.h>
#include <stdlib.h>
#include <stdio.h>

// include SSE intrinsics
#include <immintrin.h>

#define N 500000000
#define V_SIZE 4

__attribute__((aligned(16))) float data[N];

double sum(float* d, long int n) {
    double f = 0.0;
    int i;

    __m128 sum = _mm_setzero_ps();
    __m128 aux;
    float partial[V_SIZE];

    // Last elements unalligned
    for (i = 0; i < n - n%V_SIZE; i += V_SIZE) {
    	aux = _mm_loadu_ps((float *) (d+i));
    	sum = _mm_add_ps(sum, aux);
    }

    _mm_storeu_ps((float *) partial, sum);

    // Add partial vector elements
    for (i = 0; i < V_SIZE; i++) {
    	f += partial[i];
    }

    for (i = n - n%V_SIZE; i < n; i++) {
    	f += d[i];
    }

    return f;
}

int main() {
    for(int i = 0; i < N; ++i) 
        data[i] = ((float)rand())/(float)RAND_MAX - 0.5;
    clock_t t = clock();
    float total = sum(data, N);
    t = clock() - t;
    double time = ((double)t)/CLOCKS_PER_SEC;
    printf("%fs, %.0fM rows/s, %f\n\n", time, N/time/1e6, total);
}
