#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define N 500000000
float data[N];

double sum(float* d, long int n) {
    double f = 0.0;
    for(int i = 0;i < n; ++i)
        f += d[i];
    return f;
}

int main() {
    for(int i = 0; i < N; ++i) 
        data[i] = ((float)rand())/RAND_MAX - 0.5;
    clock_t t = clock();
    float total = sum(data, N);
    t = clock() - t;
    double time = ((double)t)/CLOCKS_PER_SEC;
    printf("%fs, %.0fM rows/s, %f\n\n", time, N/time/1e6, total);
}
