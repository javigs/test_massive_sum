// Compile with -mavx
// For testing purposes: clang t_avx.c -O3 -mavx -fassociative-math -fno-trapping-math -o t_avx

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#if defined(__AVX__)
    #include <immintrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
    #warning NO AVX
    #include <x86intrin.h>
#endif


#define N 500000000
#define V_SIZE 8

//_attribute__((aligned(32))) float data[N];
float data[N];

double sum(float* d, long int n) {
    double f = 0.0;
    int i;

    __m256 suma = _mm256_setzero_ps();
    __m256 aux;
    float partial[V_SIZE];

    for (i = 0; i < n - n%V_SIZE; i += V_SIZE) {
    	aux = _mm256_loadu_ps((float *) (d+i));
    	suma = _mm256_add_ps(suma, aux);
    }

    _mm256_storeu_ps((float *) partial, suma);

    // Add partial vector elements
    //f += _mm512_reduce_add_ps(suma);
    for (i = 0; i < V_SIZE; i++) {
    	f += partial[i];
    }

    // Last elements unalligned
    for (i = n - n%V_SIZE; i < n; i++) {
    	f += d[i];
    }

    return f;
}

int main() {
    for(int i = 0; i < N; i++)
        data[i] = ((float)rand())/(float)RAND_MAX - 0.5;

    clock_t t = clock();
    float total = sum(data, N);
    t = clock() - t;
    double time = ((double)t)/CLOCKS_PER_SEC;
    printf("%fs, %.0fM rows/s, %f\n\n", time, N/time/1e6, total);

    return 0;
}
