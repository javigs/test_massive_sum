// Compile with -mavx512f
// My microprocessor microcode maybe it is hijacked and doesn't run AVX512

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include <immintrin.h>


#define N 500000000
#define V_SIZE 16

float data[N];

double sum(float* d, long int n) {
    double f = 0.0;
    int i;

    __m512 suma = _mm512_setzero_ps();
    __m512 aux;
    float partial[V_SIZE];

    for (i = 0; i < n - n%V_SIZE; i += V_SIZE) {
    	aux = _mm512_loadu_ps((float *) (d+i));
    	suma = _mm512_add_ps(suma, aux);
    }

    _mm512_storeu_ps((float *) partial, suma);

    // Add partial vector elements
    f += _mm512_reduce_add_ps(suma);

    // Last elements unalligned
    for (i = n - n%V_SIZE; i < n; i++) {
    	f += d[i];
    }

    return f;
}

int main() {
    for(int i = 0; i < N; i++)
        data[i] = ((float)rand())/(float)RAND_MAX - 0.5;

    clock_t t = clock();
    float total = sum(data, N);
    t = clock() - t;
    double time = ((double)t)/CLOCKS_PER_SEC;
    printf("%fs, %.0fM rows/s, %f\n\n", time, N/time/1e6, total);

    return 0;
}
